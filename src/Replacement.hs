{-# LANGUAGE OverloadedStrings, DeriveGeneric, RecordWildCards #-}

module Replacement(
    ReplacementRule(..),
    doReplace
    ) where

import GHC.Generics
import Data.Aeson
import Data.Aeson.TH
import Data.List.Utils -- http://stackoverflow.com/a/14908602/5415895
import Data.Char


{- ReplacementRule -}

data ReplacementRule = ReplacementRule { 
    original :: String, 
    replacementWord :: String, 
    replaceWith :: Either ReplacementRule String 
    } deriving (Show)

-- http://blog.raynes.me/blog/2012/11/27/easy-json-parsing-in-haskell-with-aeson/
instance FromJSON ReplacementRule where
    parseJSON (Object v) = 
        ReplacementRule <$>
        (v .: "original")         <*>
        (v .: "replacement_word") <*>
        (v .: "replace_with")    


-- http://hackage.haskell.org/package/MissingH-0.18.6/docs/Data-List-Utils.html#v%3Areplace
-- https://www.haskell.org/hoogle/?hoogle=readFile
doReplace :: ReplacementRule -> IO String
doReplace (ReplacementRule {original = o, 
            replacementWord = w, 
            replaceWith = r}) = do 
                                  x1 <- subrep r
                                  x2 <- readFile o
                                  let x3 = replace w (map toLower w) x2
                                  return (replace (map toLower w) x1 x3)
                                  where
                                      subrep (Right a) = return a :: IO String
                                      subrep (Left a) = doReplace a
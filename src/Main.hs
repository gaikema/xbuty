module Main where
        
import System.Environment
import Replacement
import Data.Functor
import Data.Aeson
import qualified Data.ByteString.Lazy as B

-- Wrong file.
jsonFile :: FilePath
jsonFile = "/home/matt/Desktop/Link to Programming/haskell/XButY/Tests/replace.json"

-- JSON bytestring.
getJSON :: IO B.ByteString
getJSON = B.readFile jsonFile

main :: IO()
{- 
main = do
    args <- getArgs
    -- Somehow use decode
    str <- (fmap eitherDecode getJSON) :: IO (Either String ReplacementRule)
    case str of
         Left err -> putStrLn err
         Right ps -> print (ps :: ReplacementRule)
-}
rep = ReplacementRule {original = "/home/matt/Dropbox/Programming/haskell/XButY/Tests/number1.txt",
                      replacementWord = "One",
                      replaceWith = Right "dog"}

test :: IO()
test = do x <- doReplace rep
          print x
main = test

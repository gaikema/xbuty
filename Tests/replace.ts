declare module namespace {

    export interface Replacement {
        // A file with the original text. 
		original: string;
		// Replace all occurences of this word.
        replacement_word: string;
		// ... with the contents of this file, or the results of another replacement.
        replace_with: Replacement;
    }

}
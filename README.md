# XButY
Programatically generate a copypasta of the form "X but Y".

* [Example](https://www.youtube.com/watch?v=E6iN6VTL7v8&t=175s)

---

## Resources
* https://hackage.haskell.org/package/aeson-1.0.2.1/docs/Data-Aeson.html#t:FromJSON
* https://hackage.haskell.org/package/aeson-1.0.2.1/docs/Data-Aeson-TH.html
* [From a file](https://www.schoolofhaskell.com/school/starting-with-haskell/libraries-and-frameworks/text-manipulation/json#from-a-file)